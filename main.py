import os
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()

validation_rules = {
    "version": {
        "type": int,
        "min": 0,
        "max": 3
    },
    "name": {
        "type": str,
        "min": None,
        "max": None
    },
    "allowed_hosts": {
        "type": str,
        "min": None,
        "max": None
    }
}

# Function to validate an environment variable
def validate_env_variable(var_name, var_value, rules):
    if var_name not in rules:
        return False

    var_type = rules[var_name]["type"]
    var_min = rules[var_name]["min"]
    var_max = rules[var_name]["max"]

    if var_type == int:
        try:
            var_value = int(var_value)
        except ValueError:
            return False

    if var_min is not None and var_value < var_min:
        return False

    if var_max is not None and var_value > var_max:
        return False

    return True


env_dict = {
    "version": os.getenv("version", ""),
    "name": os.getenv("name", ""),
    "allowed_hosts": os.getenv("allowed_hosts", "")
}


def validate_env_variables(env_dict, rules):
    for var_name, var_value in env_dict.items():
        if not validate_env_variable(var_name, var_value, rules):
            return False
    return True

# Validate the environment variables
if validate_env_variables(env_dict, validation_rules):
    print("All environment variables are valid.")
else:
    print("Some environment variables are invalid.")

# Check the type of 'version'
if isinstance(env_dict["version"], int):
    print("The 'version' environment variable is Valid.")
else:
    print("The 'version' environment variable is Wrong Input.")

# Check the type of 'name'
if isinstance(env_dict["name"], str):
    print("The 'name' environment variable is Valid.")
else:
    print("The 'name' environment variable is Wrong Input.")

# Check the type of 'allowed_hosts'
if isinstance(env_dict["allowed_hosts"], str):
    print("The 'allowed_hosts' environment variable is Valid.")
else:
    print("The 'allowed_hosts' environment variable is Wrong Input.")
